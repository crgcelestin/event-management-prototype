# Event Management Prototype

## Table of Contents

- [Features](#features)
- [User Roles](#user-roles)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)

## Features

1. **User Registration**: Users can register with the application and select their role as admin, organizer, or volunteer. The registration process requires basic user information such as name, email, and password.

2. **User Authentication**: EEA provides secure user authentication to ensure that only authorized users can access the application. User login requires a valid email and password.

3. **Event RSVP**: Users can browse and RSVP for upcoming events. This feature allows users to indicate their interest and attendance for specific events.

4. **Event Management**: Admin and organizers have the ability to create, update, read, and delete events. They can provide event details such as name, date, time, location, description, and any additional information.

5. **Task Organization**: Users can create task lists specific to each event. This feature helps in organizing and assigning tasks to different users or teams. Tasks can be marked as completed and tracked for progress.

6. **Notifications**: Users receive notifications regarding event updates, changes, or any important information related to their role or the events they are involved in.

## User Roles

1. **Admin**: The admin role has full access and control over the application. They can manage users, create, update, and delete events, and have access to all features and functionalities.

2. **Organizer**: Organizers have the ability to create, update, and delete events. They can manage tasks, view RSVPs, and communicate with volunteers. However, they do not have administrative privileges such as user management.

3. **Volunteer**: Volunteers can RSVP for events, view event details, and access task lists. They can mark tasks as complete and communicate with organizers or other volunteers. Volunteers have limited access compared to admins and organizers.

## Installation

To run the client-side application locally, clone this repo and open a terminal in the /client directory. If this is the first time you are running the application, first run "npm install" (to install dependencies) and then "npm run prepare" (to set up Material UI themes and compile .scss). After you've done that, "npm run dev" is all you need to do to get a local server working.

## Installing Docker

Depending on your operating system, follow the specific link at https://docs.docker.com/compose/install/

## Running Docker/API server

After installing Docker, cd into eventexperenceapp. Run docker compose up.
When done with testing, run docker compose down. This will free up some resources on your computer

## Usage

Once the application is installed and running, users can perform the following actions:

1. Register a new account with the appropriate role (admin, organizer, or volunteer).
2. Log in with valid credentials.
3. Browse upcoming events and RSVP for those they are interested in.
4. Create, update, and delete events (admin and organizers only).
5. Create task lists for events and assign tasks to volunteers.
6. Mark tasks as complete and track progress.
7. Receive notifications about event updates and changes.
8. Communicate with other users through in-app messaging.

## Installing Black Formatter

[Black Docs Link](https://black.readthedocs.io/en/stable/)

> - Black is a PEP 8 compliant opinionated formatter

### Black Installation

1. Black requires Python 3.7+ and can be installed by running `pip install black`

   - Viewing python version can be done by performing `python --version`
   - Upgrading python via instructions @ [link](https://blog.enterprisedna.co/how-to-update-python/)

2. In order to get started with current files, run `black {source_file_or_directory}`
   - can also run _Black_ as a package via `python -m black {source_file_or_directory}`
