# git fac
## rename branch
git branch -m <old> <new>
git push -u origin <new-branch-name>

## create new branch
git checkout -b <branch_name>

## delete branch
git branch -d <branch_name>
