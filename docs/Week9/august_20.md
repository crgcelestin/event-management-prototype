# August 14th

### FA DOCS
- [Bigger Apps](https://fastapi.tiangolo.com/tutorial/bigger-applications/#dependencies)
* Notes
    - dependencies.py
        * [dependencies](../../server/dependencies.py)

### Dependency Management
- [PY Poetry](https://python-poetry.org/)

### Microservice Architecture
- [Microservice Docs](https://dev.to/paurakhsharma/microservice-in-python-using-fastapi-24cc)
